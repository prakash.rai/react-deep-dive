
// const a = 10;
// console.log(a);
// const a = 100;
// console.log(a)
// --> give Syantax error whole code not run due to syntax error const not redeclear again

// const a = 10;
// console.log(a);
// a = 100;
// console.log(a)
// --> this will give a type error const not reassign again

// let a = 10;
// console.log(a)
// a = 1000;
// console.log(a)
// -->code work let reassign work

// let a = 10;
// console.log(a)
// let a = 20;
// console.log(a)
// -->synatx error let can not be redeclear

// console.log(a);
// let a = 10;
// --> give refrenceError because we can not use variable in temporal dead zone

// console.log(a)
// var a = 10;
// -->code run give value undefined

// const b;
// b = 10;
// console.log(b)
// -->SyntaxError: Missing initializer in const declaration

// let a;
// a = 100;
// console.log(a)
// --> work code let can be insilize after declearation